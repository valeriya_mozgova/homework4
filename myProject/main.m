//
//  main.m
//  myProject
//
//  Created by Lera on 16.11.15.
//  Copyright © 2015 Valeriya.Mozgovaya. All rights reserved.
//

#import <Foundation/Foundation.h>
int numberPow(int number);
int enteredValue = 21; //global variable
void isEven();

int main(int argc, const char * argv[]) {
    @autoreleasepool {
       // numberPow(enteredValue);
        isEven();
    }
    return 0;
}
int numberPow(int number){
    // local variable
    int result = enteredValue * enteredValue;
    NSLog(@"The number %i in second pow = %i", enteredValue, result);
    return result;
}
// Function checks if result of numberPow is Even or Odd
void isEven(){
    int verifiedValue = numberPow(enteredValue);
    if (verifiedValue % 2 == 0)
    { NSLog(@" verifiedValue %i  is Even", verifiedValue);}
    else
    { NSLog(@" verifiedValue %i  is Odd", verifiedValue);}
}
